import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';


//imports de paginas geradas
import { TelaPlayPage } from '../pages/tela-play/tela-play';
import { TelaEscolheAvatarPage } from '../pages/tela-escolhe-avatar/tela-escolhe-avatar';
import { TelaEscolheJogoPage } from '../pages/tela-escolhe-jogo/tela-escolhe-jogo';

import { HdPlayPage } from '../pages/hd-play/hd-play';
import { HdJogoPage } from '../pages/hd-jogo/hd-jogo';
import { HdAjudaPage } from '../pages/hd-ajuda/hd-ajuda';

import { JogosProvider } from '../providers/jogos/jogos';
import { QuizProvider } from '../providers/quiz/quiz';
import { EmpresaFasesProvider } from '../providers/empresa-fases/empresa-fases';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TelaPlayPage,
    TelaEscolheAvatarPage,
    TelaEscolheJogoPage,
    HdPlayPage,
    HdJogoPage,
    HdAjudaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TelaPlayPage,
    TelaEscolheAvatarPage,
    TelaEscolheJogoPage,
    HdPlayPage,
    HdJogoPage,
    HdAjudaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    EmpresaFasesProvider,
    QuizProvider,
    JogosProvider
  ]
})
export class AppModule {}
