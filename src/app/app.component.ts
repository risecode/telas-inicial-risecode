import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ConfProvider } from '../providers/conf/conf';
import { EmpresaFasesProvider } from '../providers/empresa-fases/empresa-fases';
import { QuizProvider } from '../providers/quiz/quiz';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html',
  providers: [
    ConfProvider,
    EmpresaFasesProvider,
    QuizProvider
  ]
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, ConfProvider:ConfProvider,
    EmpresaFasesProvider:EmpresaFasesProvider,public QuizProvider:QuizProvider) {
    platform.ready().then(() => {

      let config =  ConfProvider.getConfigData();
      if(config==null){
        ConfProvider.setConfigData(true,"Júnior",0);
        EmpresaFasesProvider.setConfigData("0");
      }else{
        this.rootPage = HomePage;
      }
      console.log(config);

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

