import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { JogosProvider } from '../../providers/jogos/jogos';


let cont = 50;

let ct = 100;

let pontos=0;

@IonicPage()
@Component({
  selector: 'page-hd-jogo',
  templateUrl: 'hd-jogo.html',
  providers:[
    JogosProvider
  ]
})
export class HdJogoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
     public alertCtrl: AlertController,public JogosProvider:JogosProvider) {
  }

  ionViewDidLoad() {
    pontos=0;
    ct=100;
    cont=50;

    console.log('ionViewDidLoad HdjogoPage');
    document.getElementById('cesta').style.position = "absolute";
    document.getElementById('cesta').style.left = cont + "px";
    document.getElementById('bt').style.position = "absolute";
    document.getElementById('bt').style.left = 20 + "%";
    document.getElementById('gc').style.position = "absolute";
    document.getElementById('gc').style.left = 40 + "%";
    document.getElementById('pt').style.position = "absolute";
    document.getElementById('pt').style.left = 60 + "%";
    document.getElementById('hd').style.position = "absolute";
    document.getElementById('hd').style.left = 80 + "%";


    let bg = document.getElementById('corpo');

    bg.addEventListener('click', function (e: MouseEvent) {

      document.getElementById('cesta').style.position = "absolute";

      let cesta = document.getElementById('cesta');
      let bg = document.getElementById('corpo');

      let xPosition = e.clientX - bg.getBoundingClientRect().left - (cesta.clientWidth / 2);
      cesta.style.left = xPosition + "px";
    });

    this.sleep();
   
  }
  private async sleep() {

    let yCesta = document.getElementById('cesta').offsetTop;
    let widthCesta = document.getElementById('cesta').offsetWidth;

    while (true) {
      ct -= 4;
      document.getElementById('bt').style.bottom = ct + "%";
      document.getElementById('gc').style.bottom = ct + "%";
      document.getElementById('pt').style.bottom = ct + "%";
      document.getElementById('hd').style.bottom = ct + "%";
      await this.delay(100);

      let yB = document.getElementById('bt').offsetTop;
      let xB = document.getElementById('bt').offsetLeft;
      let yG = document.getElementById('gc').offsetTop;
      let xG = document.getElementById('gc').offsetLeft;
      let yP = document.getElementById('gc').offsetTop;
      let xP = document.getElementById('gc').offsetLeft;
      let yH = document.getElementById('hd').offsetTop;
      let xH = document.getElementById('hd').offsetLeft;

      let xCesta = document.getElementById('cesta').offsetLeft;

      if (yB >= yCesta || yG >= yCesta || yP >= yCesta || yH >= yCesta) {

        if (xB >= xCesta && xB <= xCesta+widthCesta || 
          xG >= xCesta && xG <= xCesta+widthCesta || 
          xP >= xCesta && xP <= xCesta+widthCesta) {
            const alert = this.alertCtrl.create({
              title: 'Não foi dessa vez!',
              subTitle: 'Você perdeu',
              buttons: ['OK']
            });
            alert.present();
          break;
        } else {

          if(xH >= xCesta && xH <= xCesta+widthCesta){
            pontos++;
            let aviso = document.getElementById('aviso').innerHTML = "Pontos atuais: "+pontos;
            if(pontos==10){
              const alert = this.alertCtrl.create({
                title: 'Parabéns!',
                subTitle: 'Você venceu',
                buttons: ['OK']
              });
              alert.present();
              this.JogosProvider.setDataHD();
              break;
            }
          }

          let n1 = this.pick(10, 80);
          document.getElementById('bt').style.left = n1 + "%";
          let n2 = this.pick(10, 80);
          document.getElementById('gc').style.left = n2 + "%";
          let n3 = this.pick(10, 80);
          document.getElementById('pt').style.left = n3 + "%";
          let n4 = this.pick(10, 80);
          

          while(n1==n4 || n2==n4 || n3==n4){
            n4 = this.pick(10, 80);
          }

          document.getElementById('hd').style.left = n4 + "%";

          ct = 100;

        }
      }
    }
    this.navCtrl.pop();
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  pick(min, max) {
    var value = Math.floor(Math.random() * max + min);
    return value;
  }

}