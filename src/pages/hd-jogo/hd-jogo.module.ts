import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HdJogoPage } from './hd-jogo';

@NgModule({
  declarations: [
    HdJogoPage,
  ],
  imports: [
    IonicPageModule.forChild(HdJogoPage),
  ],
})
export class HdJogoPageModule {}
