import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TelaPlayPage } from '../tela-play/tela-play';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  //funcao para mudar para tela de play assim que o logo da empresa dev for apresentado
  mudarDePagina () {
    this.navCtrl.push(TelaPlayPage)
  }

  //acoes feitas assim que a pagina for carregada
  ionViewDidLoad() {
    
  
    //muda de pagina apos 3000ms
    setTimeout(() => this.mudarDePagina(), 3200);
  }
}
