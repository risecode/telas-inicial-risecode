import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { TelaEscolheJogoPage } from '../tela-escolhe-jogo/tela-escolhe-jogo';


@IonicPage()
@Component({
  selector: 'page-tela-escolhe-avatar',
  templateUrl: 'tela-escolhe-avatar.html',
})
export class TelaEscolheAvatarPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  personagem_selecionado = 0;
  @ViewChild(Slides) slides: Slides;

  ionViewDidLoad() {
    
  }

  mudarPersonagemDireita(){
    if(this.personagem_selecionado==5){
      this.personagem_selecionado = 0;
      this.slides.slideTo(this.personagem_selecionado, 500);
    } else{
      this.personagem_selecionado = this.personagem_selecionado+1;
      this.slides.slideTo(this.personagem_selecionado, 500);
    }
  }

  mudarPersonagemEsquerda(){
    if(this.personagem_selecionado==0){
      this.personagem_selecionado = 5;
      this.slides.slideTo(this.personagem_selecionado, 500);
    } else{
      this.personagem_selecionado = this.personagem_selecionado-1;
      this.slides.slideTo(this.personagem_selecionado, 500);
    }
  }
  

  
    //setTimeout(() =>  this.mudaPersonagem(), 1100);
   
  proxPag(){
    this.navCtrl.push(TelaEscolheJogoPage);
  }

}
