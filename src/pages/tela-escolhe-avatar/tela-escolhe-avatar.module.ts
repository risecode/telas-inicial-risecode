import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelaEscolheAvatarPage } from './tela-escolhe-avatar';

@NgModule({
  declarations: [
    TelaEscolheAvatarPage,
  ],
  imports: [
    IonicPageModule.forChild(TelaEscolheAvatarPage),
  ],
})
export class TelaEscolheAvatarPageModule {}
