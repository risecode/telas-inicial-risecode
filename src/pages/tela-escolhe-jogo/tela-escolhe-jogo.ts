import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HdPlayPage } from '../hd-play/hd-play';

/**
 * Generated class for the TelaEscolheJogoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tela-escolhe-jogo',
  templateUrl: 'tela-escolhe-jogo.html',
})
export class TelaEscolheJogoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TelaEscolheJogoPage');
  }

  abreJogo1(){
    this.navCtrl.push(HdPlayPage);
  }


}
