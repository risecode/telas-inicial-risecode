import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelaEscolheJogoPage } from './tela-escolhe-jogo';

@NgModule({
  declarations: [
    TelaEscolheJogoPage,
  ],
  imports: [
    IonicPageModule.forChild(TelaEscolheJogoPage),
  ],
})
export class TelaEscolheJogoPageModule {}
