import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HdJogoPage } from '../hd-jogo/hd-jogo';
import { HdAjudaPage } from '../hd-ajuda/hd-ajuda';

/**
 * Generated class for the HdPlayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hd-play',
  templateUrl: 'hd-play.html',
})
export class HdPlayPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HdPlayPage');
  }

  chamarProxTelaPlay(){
   this.navCtrl.push(HdJogoPage);
  }

  chamarProxTelaAjuda(){
    this.navCtrl.push(HdAjudaPage)
  }

}
