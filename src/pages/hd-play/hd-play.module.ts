import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HdPlayPage } from './hd-play';

@NgModule({
  declarations: [
    HdPlayPage,
  ],
  imports: [
    IonicPageModule.forChild(HdPlayPage),
  ],
})
export class HdPlayPageModule {}
