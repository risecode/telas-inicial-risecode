import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HdPlayPage } from '../hd-play/hd-play';

/**
 * Generated class for the HdAjudaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hd-ajuda',
  templateUrl: 'hd-ajuda.html',
})
export class HdAjudaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HdAjudaPage');
  }

  voltar(){
    this.navCtrl.push(HdPlayPage);
  }

}
