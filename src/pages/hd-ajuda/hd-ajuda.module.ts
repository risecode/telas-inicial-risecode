import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HdAjudaPage } from './hd-ajuda';

@NgModule({
  declarations: [
    HdAjudaPage,
  ],
  imports: [
    IonicPageModule.forChild(HdAjudaPage),
  ],
})
export class HdAjudaPageModule {}
