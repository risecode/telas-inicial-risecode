import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelaPlayPage } from './tela-play';

@NgModule({
  declarations: [
    TelaPlayPage,
  ],
  imports: [
    IonicPageModule.forChild(TelaPlayPage),
  ],
})
export class TelaPlayPageModule {}
