import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TelaEscolheAvatarPage } from '../tela-escolhe-avatar/tela-escolhe-avatar';

/**
 * Generated class for the TelaPlayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tela-play',
  templateUrl: 'tela-play.html',
})
export class TelaPlayPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  px = 580;
  controladorPisca = false;

  sobeBalao(){
    this.px = this.px - 8
    if(this.px>50){
      document.getElementById("balao").style.top = this.px +"px";  
    } else {
      document.getElementById("logo").style.display = "block";
      this.controladorPisca = true;
    }      
  }

  piscaToqueJogar(){
    if(this.controladorPisca==true){
      if(document.getElementById("toque").style.display == "block"){
        document.getElementById("toque").style.display = "none";
      } else {
        document.getElementById("toque").style.display = "block";
      }
    } 
  }

  liberarProxTela(){
    document.getElementById("btn-toque").style.display = "block";
  }

  ionViewDidLoad() {
    document.getElementById("logo").style.display = "none";
    document.getElementById("toque").style.display = "none";
    document.getElementById("btn-toque").style.display = "none";
    
    var i = 0;
    
      setInterval(() => this.sobeBalao(), 100);
      setInterval(() => this.piscaToqueJogar(), 500);
      setInterval(() => this.liberarProxTela(), 6800);
         
     
  }

  chamarProxTela(){
    this.navCtrl.push(TelaEscolheAvatarPage)
  }

}
