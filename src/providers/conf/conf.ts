import { Injectable } from '@angular/core';

const configKey:string = "config";

@Injectable()
export class ConfProvider {

  constructor() {
 
  }

  getConfigData():JSON{
    let items;
    if (localStorage.getItem(configKey)) {
      items = JSON.parse(localStorage.getItem(configKey));
    } else {
      items = null;
    }

    return items;
  }

  setConfigData(showSlide?:boolean,level?:string,percentage?:number){
    let config = {
      showSlide:true,
      level:"Júnior",
      percentage:0
    }

    if(showSlide!=null){
      config.showSlide = showSlide;
    }

    if(level!=null){
      config.level = level;
    }

    if(percentage!=null){
      config.percentage = percentage;
    }

    localStorage.setItem(configKey, JSON.stringify(config));
  }

}
