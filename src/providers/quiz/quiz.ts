import { Injectable } from '@angular/core';
import { ConfProvider } from '../conf/conf';

const staticKey: string = "static";
const confirmKey: string = "confirm";
let value;

@Injectable()
export class QuizProvider {

  constructor(public confPrivider:ConfProvider) {
    console.log('Hello QuizProvider Provider');
  }

  getData(): number {
    let items;
    if (localStorage.getItem(staticKey)) {
      items = parseInt(localStorage.getItem(staticKey));
    } else {
    items = null;
    }
    return items;
  }

  getConfirmation(): boolean {
    let items;
    if (localStorage.getItem(confirmKey)) {
      items = JSON.parse(localStorage.getItem(confirmKey));
    } else {
    items = null;
    }
    return items;
  }


  setData(fase) {
    localStorage.setItem(staticKey, JSON.stringify(fase));

    if(fase==10 && this.getConfirmation()==null){
      localStorage.setItem(staticKey, JSON.stringify(0));
      localStorage.setItem(confirmKey, JSON.stringify(false));
      value = this.confPrivider.getConfigData();

      let lvl = value.level;
      let pct = value.percentage + 2.5;

      this.confPrivider.setConfigData(true,lvl,pct);
    }
  }


}
