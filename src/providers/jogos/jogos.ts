import { Injectable } from '@angular/core';
import { ConfProvider } from '../conf/conf';

const hdKey: string = "hd";
let value;

@Injectable()
export class JogosProvider {
  

  constructor(public confProvider: ConfProvider) {
    console.log('Hello JogosProvider Provider');
  }

  getDataHD(): string {
    if (localStorage.getItem(hdKey)) {
      return localStorage.getItem(hdKey);
    } else {
      return null;
    }
  }

  setDataHD(){
    if(this.getDataHD()==null){
      localStorage.setItem(hdKey, "foi");
      value = this.confProvider.getConfigData();

      let lvl = value.level;
      let pct = value.percentage + 2.5;

      this.confProvider.setConfigData(true,lvl,pct);
    }
  }

}
