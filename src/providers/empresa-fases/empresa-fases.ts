import { Injectable } from '@angular/core';
import { ConfProvider } from '../conf/conf';

const fasesKey: string = "fases";
const imgsKey: string = "imgs";
let fasesI;
let it;
let it2;

@Injectable()
export class EmpresaFasesProvider {

  constructor(public confiProvider: ConfProvider) {
  }

  getData(): JSON {
    let items;
    if (localStorage.getItem(fasesKey)) {
      items = JSON.parse(localStorage.getItem(fasesKey));
    } else {
    items = null;
    }

    return items;
  }

  getImgs(): JSON {
    let imgs = {
      fase1: "one",
      fase2: "twoantes",
      fase3: "threeantes",
      fase4: "fourantes",
      fase5: "fiveantes",
      fase6: "sixantes",
      fase7: "sevenantes",
      fase8: "eightantes",
      fase9: "nineantes"
    }

    fasesI = this.getData();

    imgs.fase1 = "one";

    if (fasesI.fase1 == true) {
      imgs.fase2 = "two";
    }
    if (fasesI.fase2 == true) {
      imgs.fase3 = "three";
    }
    if (fasesI.fase3 == true) {
      imgs.fase4 = "four";
    }
    if (fasesI.fase4 == true) {
      imgs.fase5 = "five";
    }
    if (fasesI.fase5 == true) {
      imgs.fase6 = "six";
    }
    if (fasesI.fase6 == true) {
      imgs.fase7 = "seven";
    }
    if (fasesI.fase7 == true) {
      imgs.fase8 = "eight";
    }
    if (fasesI.fase8 == true) {
      imgs.fase9 = "nine";
    }

    localStorage.setItem(imgsKey, JSON.stringify(imgs));

    let items;

    items = JSON.parse(localStorage.getItem(imgsKey));

    return items;
  }

  setConfigData(fase) {

    it = this.getData();
    it2 = this.confiProvider.getConfigData();
    let fases;

      fases = {
        fase1: false,
        fase2: false,
        fase3: false,
        fase4: false,
        fase5: false,
        fase6: false,
        fase7: false,
        fase8: false,
        fase9: false

  }

  if(it!=null){
    fases.fase1 = it.fase1,
    fases.fase2 = it.fase2,
    fases.fase3 = it.fase3,
    fases.fase4 = it.fase4,
    fases.fase5 = it.fase5,
    fases.fase6 = it.fase6,
    fases.fase7 = it.fase7,
    fases.fase8 = it.fase8,
    fases.fase9 = it.fase9

  console.log(fases);

}

    let foi=false;

    let percentage;
    let level;
    if (fase == "1") {
      if (it.fase1 != true) {
        fases.fase1 = true;
        foi=true;
      }
    } else if (fase == "2") {
      if (it.fase2 != true) {
        fases.fase2 = true;
        foi=true;
      }
    } else if (fase == "3") {
      if (it.fase3 != true) {
        fases.fase3 = true;
        foi=true;
      }
    } else if (fase == "4") {
      if (it.fase4 != true) {
        fases.fase4 = true;
        foi=true;
      }
    } else if (fase == "5") {
      if (it.fase5 != true) {
        fases.fase5 = true;
        foi=true;
      }
    }
    else if (fase == "6") {
      if (it.fase6 != true) {
        fases.fase6 = true;
        foi=true;
      }
    }
    else if (fase == "7") {
      if (it.fase7 != true) {
        fases.fase7 = true;
        foi=true;
      }
    } else if (fase == "8") {
      if (it.fase8 != true) {
        fases.fase8 = true;
        foi=true;
      }
    }
    else if (fase == "9") {
      if (it.fase9 != true) {
        fases.fase9 = true;
        foi=true;
      }
    }
    if(foi==true){
      percentage = it2.percentage + 10;
      if(percentage<=40){
        level = "Júnior";
      }else if(percentage>=50 && percentage<80){
        level = "Pleno";
      }else{
        level = "Sênior";
      }
      this.confiProvider.setConfigData(true, level, percentage);
    }
    localStorage.setItem(fasesKey, JSON.stringify(fases));
  }


}
